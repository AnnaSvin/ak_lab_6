#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <conio.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	signed short a, b, x_c, x_asm;
	printf("������ �������� ������:\n");
	printf("a = "); scanf_s("%d", &a);
	printf("b = "); scanf_s("%d", &b);
	if (a > b)
		x_c = b / a - 1;
	else if (a < b)
		x_c = (a * b - 9) / b;
	else x_c = -120;
	printf("��������� �� ��� C++: X = %d\n", x_c);
	_asm
	{
		mov ax, a;
		mov bx, b;
		cmp ax, bx;
		jg mark1;
		je mark2;
		jl mark3;
	mark1:
		xchg ax, bx;
		cwd;
		idiv bx;
		dec ax;
		mov x_asm, ax;
		jmp exit1;
	mark2:
		mov x_asm, -120;
		jmp exit1;
	mark3:
		imul bx;
		mov cx, 1;
		idiv cx;
		sub ax, 9;
		cwd;
		mov bx, b;
		idiv bx;
		mov x_asm, ax;
		jmp exit1;
	exit1:
	}
	printf("��������� �� ��� Assembler X = %d\n", x_asm);
	return 0;
}